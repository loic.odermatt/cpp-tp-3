# TP3 - cpp-closestneighbours

## Objectifs

• Implémentation de l’algorithme Closest Pairs

• Utilisation de tests unitaires

• Surcharge d‘opérateurs 

## Spécifications

Dans cet exercice nous allons implémenter un algorithme vu au cours algorithmique 3, et
l’appliquer sur un problème d’analyse de données.

