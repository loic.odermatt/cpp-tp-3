//
// Created by Loic Odermatt on 05.04.22.
//

#include "Point.h"

Point::Point(Point &point){
    this->x = point.getX();
    this->y = point.getY();
    this->name = point.getName();
}

Point::Point(double x, double y, std::string name){
    this->x = x;
    this->y = y;
    this->name = name;
}

double Point::getX() const {
    return this->x;
}

double Point::getY() const {
    return this->y;
}

std::string Point::getName(){
    return this->name;
}

void Point::setX(double x){
    this->x = x;
}

void Point::setY(double y){
    this->y = y;
}

double Point::distance(const Point& point) const {
    return 0.0;
}

Point& Point::operator+=(const Point &other){
    this->x += other.getX();
    this->y += other.getY();
    return *this;
}

Point& Point::operator-=(const Point &other){
    this->x -= other.getX();
    this->y -= other.getY();
    return *this;
}

Point& Point::operator*=(double other){
    this->x *= other;
    this->y *= other;
    return *this;
}

Point& Point::operator/=(double other){
    this->x /= other;
    this->y /= other;
    return *this;
}

Point Point::operator+(const Point &other){
    double tempx = this->x + other.getX();
    double tempy = this->y + other.getY();

    Point p(tempx, tempy);
    return p;
}

Point Point::operator-(const Point &other){
    double tempx = this->x - other.getX();
    double tempy = this->y - other.getY();

    Point p(tempx, tempy);
    return p;
}

Point Point::operator*(double other){
    double tempx = this->x * other;
    double tempy = this->y * other;

    Point p(tempx, tempy);
    return p;
}

Point Point::operator/(double other){
    double tempx = this->x / other;
    double tempy = this->y / other;

    Point p(tempx, tempy);
    return p;
}

